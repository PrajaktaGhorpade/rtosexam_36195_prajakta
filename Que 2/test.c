#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
	char buf[32];
	int cnt;
	printf("linux user space process reading data .\n");
	int fd = open("/dev/rtp2", O_RDONLY);
	if(fd < 0) {
		perror("cannot open xenomai rt pipe.\n");
		_exit(1);
	}

	while( (cnt = read(fd, buf, sizeof(buf))) > 0 ) {
		write(STDOUT_FILENO, buf, cnt);
	}

	close(fd);
	return 0;
}


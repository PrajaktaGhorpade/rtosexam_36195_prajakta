#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <rtdk.h>
#include <signal.h>
#include <native/task.h>
#include <native/pipe.h>

RT_PIPE pipe1;

void sigint_handler(int sig){
    //empty handler
}

//task function
void my_task_func(void *param){
    int i,ret;
    RT_TASK_INFO info;
    //task implementation
    rt_task_inquire(NULL,&info);
    char buf[32];
    for(i=1;i<=10;i++){
        sprintf(buf,"task %s running %d(pri=%d) .\n",info.name,i,info.bprio);
        ret = rt_pipe_write(&pipe1,buf,strlen(buf)+1,P_NORMAL);
        if(ret < 0){
            rt_printf("rt_pipe_write() failed.\n");
            rt_task_delete(NULL);
            return;
        }
        rt_task_sleep(100000000);
    }
    //delete the task
    rt_task_delete(NULL);
}

int main(){
    int ret;
    struct sigaction sa;
    RT_TASK t;

     ret = mlockall(MCL_CURRENT | MCL_FUTURE);
     if(ret != 0){
         rt_printf("mlockall() failed.\n");
         _exit(1);
     }

     memset(&sa,0,sizeof(sa));
     sa.sa_handler = sigint_handler;
     ret = sigaction(SIGINT,&sa,NULL);
     if(ret != 0){
         rt_printf("sigaction() failed.\n");
         _exit(1);
     }

     //RT print enable
     rt_print_auto_init(1);

     //Rt pipe creation
     ret = rt_pipe_create(&pipe1,NULL,2,1000);
     if(ret != 0){
        rt_printf("rt_pipe_create() failed.\n");
        _exit(1);
    }

    // task creation
    ret = rt_task_create(&t,"task",0,50,T_CPU(0));
    if(ret != 0){
        rt_printf("rt_task_create() failed.\n");
        _exit(1);
    }

    //task start 
    ret = rt_task_start(&t,my_task_func,NULL);
    if(ret != 0){
        rt_printf("rt_task_start() failed.\n");
        _exit(1);
    }

    pause();
    //delete RT pipe
    rt_pipe_delete(&pipe1);
    return 0;
}
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <rtdk.h>
#include <signal.h>
#include <native/task.h>
#include <native/sem.h>

//declare semaphore
RT_SEM sem;

//signal handler
void sigint_handler(int sig){
    //empty handler
}

//implement task function
void print_table(void *param){
    int i;
    long num =(long)param;
    RT_TASK_INFO info;

    //task implementation
    rt_task_inquire(NULL,&info);

    //semaphore wait
    rt_sem_p(&sem,TM_INFINITE);

    for(i=1;i<=10;i++){
        rt_printf("%s: %ld * %ld = %ld.\n",info.name,num,i,num * i);
        rt_task_sleep(10000);
    }

    //delete the task
    rt_task_delete(NULL);
}

int main()
{
    int ret;
    struct sigaction sa;
    RT_TASK tasks[4];

     ret = mlockall(MCL_CURRENT | MCL_FUTURE);
     if(ret != 0){
         rt_printf("mlockall() failed.\n");
         _exit(1);
     }

     memset(&sa,0,sizeof(sa));
     sa.sa_handler = sigint_handler;
     ret = sigaction(SIGINT,&sa,NULL);
     if(ret != 0){
         rt_printf("sigaction() failed.\n");
         _exit(1);
     }

   
    //RT print enable
     rt_print_auto_init(1);

     //semaphore
     ret = rt_sem_create(&sem,"sem_flag",0,S_FIFO);
     if(ret != 0){
         rt_printf("rt_sem_create() failed.\n");
         _exit(1);
     }

    
    //task 1
        // task creation for task1
        ret = rt_task_create(&tasks[0],"task0",0,10,T_CPU(0));
        if(ret != 0){
            rt_printf("rt_task_create() failed.\n");
            _exit(1);
        }

        //task start for task1
        ret = rt_task_start(&tasks[0],print_table,(void*)2L);
        if(ret != 0){
            rt_printf("rt_task_start() failed.\n");
            _exit(1);
        }

    //task 2
        // task creation for task2
        ret = rt_task_create(&tasks[1],"task1",0,10,T_CPU(0));
        if(ret != 0){
            rt_printf("rt_task_create() failed.\n");
            _exit(1);
        }

        //task start for task2
        ret = rt_task_start(&tasks[1],print_table,(void*)3L);
        if(ret != 0){
            rt_printf("rt_task_start() failed.\n");
            _exit(1);
        }

    //task 3
        // task creation for task3
        ret = rt_task_create(&tasks[2],"task2",0,10,T_CPU(0));
        if(ret != 0){
            rt_printf("rt_task_create() failed.\n");
            _exit(1);
        }

        //task start for task3
        ret = rt_task_start(&tasks[2],print_table,(void*)4L);
        if(ret != 0){
            rt_printf("rt_task_start() failed.\n");
            _exit(1);
        }

    //task4
        // task creation for task1
        ret = rt_task_create(&tasks[3],"task3",0,10,T_CPU(0));
        if(ret != 0){
            rt_printf("rt_task_create() failed.\n");
            _exit(1);
        }

        //task start for task1
        ret = rt_task_start(&tasks[3],print_table,(void*)5L);
        if(ret != 0){
            rt_printf("rt_task_start() failed.\n");
            _exit(1);
        }

    pause();
    //to run task simultaneously
    rt_sem_broadcast(&sem);
    pause();
    rt_sem_delete(&sem);
    return 0;
}
TARGET = que1

XENO_CONFIG := /usr/xenomai/bin/xeno-config

CFLAGS := $(shell $(XENO_CONFIG) --xeno-cflags)
LDFLAGS := $(shell $(XENO_CONFIG) --xeno-ldflags) -lnative -lrtdk

CC := gcc   
EXECUTABLE := $(TARGET).out

all: $(EXECUTABLE)

$(EXECUTABLE): $(TARGET).c
	$(CC) -o $@ $(CFLAGS) $< $(LDFLAGS)

clean: 
	rm -f $(EXECUTABLE)